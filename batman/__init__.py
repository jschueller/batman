"""
BATMAN package
**************
"""
import openturns as ot
ot.RandomGenerator.SetSeed(123456)

__version__ = '1.7.3'
__branch__ = None
__commit__ = None
